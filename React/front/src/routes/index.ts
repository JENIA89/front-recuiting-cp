import  React  from 'react';
import Home from '../pages/Home';
import Mailing from '../pages/Mailing';

//интерфейс для route 
export interface IRoute {
    path: string
    component: React.ComponentType
    exact?: boolean
}

//перечисления для маршрутов. полезно по мере разрастания приложения
export enum RouteNames {
    HOME = '/',
    MAILING = '/mailing'
}

//роутинг
export const routes: IRoute[] = [
    {path: RouteNames.HOME, exact: true, component: Home},
    {path: RouteNames.MAILING, exact: true, component: Mailing}
]