import React from 'react'
import { Redirect, Route, Switch } from 'react-router'
import { RouteNames, routes } from '../routes'

const AppRouter =()=> {
    return (
        <Switch>
            {routes.map(route=>(
                <Route key={route.path} path={route.path} exact={route.exact} component={route.component}/>
            ))}
            <Redirect to={RouteNames.HOME}/>
        </Switch>
    )
}

export default AppRouter
